package ch03

import akka.actor.ActorSystem
import akka.actor.SupervisorStrategy.{Restart, Resume, Escalate}
import akka.testkit._
import akka.actor._
import ch02.ch02.StopSystemAfterAll
import ch3.{SupervisorSummerizeNew, SummerizeActor}
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import scala.concurrent.duration._
/**
 * Created by anhhh11 on 19/07/2014.
 */
class SupervisorAddTest
  extends TestKit(ActorSystem("testsystem")) //Create system for test
  with WordSpec // nested "something" must { "equal smt" in {} }
  with MustMatchers // provide `must`
  with StopSystemAfterAll  // provide `beforeAll`, `afterAll`
  with ImplicitSender
{
//  "SummerizeActor" must {
//    "throw stateError" in {
//      val probe = TestProbe()
//      val summerizeActor = system.actorOf(SummerizeActor.props(probe.ref))
//      summerizeActor ! "stateError"
//    }
//    "throw IllArgument" in {
//      val probe = TestProbe()
//      val summerizeActor = system.actorOf(SummerizeActor.props(probe.ref), "sa2")
//      summerizeActor ! "error"
//    }
//  }
    "An exception in child" must {
      "escalate error when failing" in {
        //restart and erase all state
        val supervisorStrategy = OneForOneStrategy(){
          case _ : Exception => Escalate
        }
        val probe = TestProbe()
        val summerizeSupervisor = system.actorOf(Props(new SupervisorSummerizeNew(probe.ref,supervisorStrategy)))
        summerizeSupervisor! "isRestarted"
        expectMsg(false)
        summerizeSupervisor ! 4
        probe.expectMsg(4)

        summerizeSupervisor ! "stateError"
        Thread.sleep(1000)

        summerizeSupervisor ! "isRestarted"
        expectMsg(true)

        summerizeSupervisor ! 4
        probe.expectMsg(4)
      }
      "Stop child when failing multiple times" in {
        val supervisorStrategy = OneForOneStrategy(maxNrOfRetries = 5, //Max number of time restart in
        withinTimeRange = 1 minute // 1 minute
        ){
          case _ : IllegalArgumentException => Resume
          case _ :Exception => Restart
        }
        val probe = TestProbe()
        val summerizeSupervisor = system.actorOf(Props(new SupervisorSummerizeNew(probe.ref,supervisorStrategy)))

        summerizeSupervisor ! 4
        probe.expectMsg(4)
        for(index<-0 until 5){
          summerizeSupervisor!"stateError"
        }
        summerizeSupervisor ! 4
        probe.expectMsg(4)

        summerizeSupervisor ! "stateError"

        Thread.sleep(1000)
        summerizeSupervisor ! "getActor"

        val childRef = expectMsgType[ActorRef](1 second)
        childRef.isTerminated must be(true)

        summerizeSupervisor ! "isChildTerminated"
        expectMsg(true)

      }
    }


  }
