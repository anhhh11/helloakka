package ch03

import akka.actor.{ActorKilledException, ActorSystem, Kill, Props}
import akka.testkit.{ImplicitSender, TestKit}
import ch02.ch02.StopSystemAfterAll
import ch3._
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import scala.concurrent.duration._
import akka.testkit.CallingThreadDispatcher
/**
 * Created by anhhh11 on 20/07/2014.
 */

class TwoLevelSupervisor extends TestKit(gen.TestConfig.system("test-system")) //Create system for test
with WordSpec // nested "something" must { "equal smt" in {} }
with MustMatchers // provide `must`
with StopSystemAfterAll // provide `beforeAll`, `afterAll`
with ImplicitSender {
  "The averageSystem" must {
    //    "be able to calculate average" in {
    //      val systemName = "averageComponent"
    //      val testActorRef = system.actorOf(Props[SupervisorAvgCmp], systemName)
    //      testActorRef ! new RequestAverage(8)
    //      testActorRef ! 1
    //      testActorRef ! 9
    //      testActorRef ! 2
    //      testActorRef ! 8
    //      testActorRef ! 3
    //      testActorRef ! 7
    //      testActorRef ! 4
    //      testActorRef ! 6
    //      expectMsg(new ResponseAverage(8, 5.0))
    //    }
    //    "be able to recover restarts" in {
    //      val systemName = "averageComponent-Restart"
    //      val evilMonkey = new EvilMonkey(system, systemName)
    //      val testActorRef = system.actorOf(Props[SupervisorAvgCmp], systemName)
    //      testActorRef ! new RequestAverage(8)
    //      testActorRef ! 1
    //      testActorRef ! 9
    //      evilMonkey.restartAverage()
    //      testActorRef ! 2
    //      testActorRef ! 8
    //      testActorRef ! 3
    //      testActorRef ! 7
    //      testActorRef ! 4
    //      evilMonkey.restartSummerize()
    //      testActorRef ! 6
    //      expectMsg(5 seconds, new ResponseAverage(8, 5.0))
    //    }
    //    "be able to send error on several restarts" in {
    //      val actorName = "averageComponent-multi-restart"
    //      val evilMonkey = new EvilMonkey(system, actorName)
    //      val testActorRef = system.actorOf(
    //        Props[SupervisorAvgCmp], actorName)
    //      testActorRef ! new RequestAverage(8)
    //      testActorRef ! 1
    //      evilMonkey.restartSummerize()
    //      testActorRef ! 9
    //      evilMonkey.restartAverage()
    //      testActorRef ! 2
    //      evilMonkey.restartSummerize()
    //      testActorRef ! 8
    //      evilMonkey.restartAverage()
    //      testActorRef ! 3
    //      evilMonkey.restartSummerize()
    //      testActorRef ! 7
    //      evilMonkey.restartAverage()
    //      testActorRef ! 4
    //      testActorRef ! 6
    //
    //      expectMsg(10 seconds, new ResponseError("Stop Error"))
    //      Thread.sleep(1000)
    //      testActorRef ! new RequestAverage(8)
    //      testActorRef ! 1
    //      testActorRef ! 9
    //      testActorRef ! 2
    //      testActorRef ! 8
    //      testActorRef ! 3
    //      testActorRef ! 7
    //      testActorRef ! 4
    //      testActorRef ! 6
    //
    //      expectMsg(5 seconds, new ResponseAverage(8, 5.0))
    //      system.stop(testActorRef)
    //    }
    "be able to send error on kill" in {
      val systemName = "avgComp-kill"
      val evilMonkey = new EvilMonkey(system, systemName)
      val testActorRef = system.actorOf(Props[SupervisorAvgCmp], systemName)
      testActorRef ! new RequestAverage(8)
      testActorRef ! 1
      testActorRef ! 9
      testActorRef ! 2
      evilMonkey.killSummerize()
      testActorRef ! 7
      testActorRef ! 4
      testActorRef ! 6
      expectMsg(10 seconds, new ResponseError("Stop Error"))
      Thread.sleep(1000) //wait it to recover
      testActorRef.isTerminated must be(false)
      testActorRef ! new RequestAverage(8)
      testActorRef ! 1
      testActorRef ! 9
      testActorRef ! 2
      testActorRef ! 8
      testActorRef ! 3
      testActorRef ! 7
      testActorRef ! 4
      testActorRef ! 6

      expectMsg(5 seconds, new ResponseAverage(8, 5.0)) //<co id="ch3-compl-test-kill-3" />
      system.stop(testActorRef)
    }
    "be able to send error on Supervisor kill" in {
      val actorName = "averageComponent-kill-super"
      val evilMonkey = new EvilMonkey(system, actorName)
      val testActorRef = system.actorOf(Props[SupervisorAvgCmp], actorName)
      testActorRef ! new RequestAverage(8)
      testActorRef ! 1
      testActorRef ! 9
      testActorRef ! 2
      testActorRef ! 8
      testActorRef ! 3
      evilMonkey.killSupervisor()
      testActorRef ! 7
      testActorRef ! 4
      testActorRef ! 6
      expectMsg(10 seconds, new ResponseError("Stop Error"))

      testActorRef ! new RequestAverage(8)
      testActorRef ! 1
      testActorRef ! 9
      testActorRef ! 2
      testActorRef ! 8
      testActorRef ! 3
      testActorRef ! 7
      testActorRef ! 4
      testActorRef ! 6

      expectMsg(5 seconds, new ResponseAverage(8, 5.0))
      system.stop(testActorRef)
    }
    "be able to detect result kill" in {
      val actorName = "averageComponent-killResult"
      val evilMonkey = new EvilMonkey(system, actorName)
      val testActorRef = system.actorOf(Props[SupervisorAvgCmp], actorName)

      testActorRef ! new RequestAverage(8)
      testActorRef ! 1
      testActorRef ! 9
      testActorRef ! 2
      testActorRef ! 8
      testActorRef ! 3
      evilMonkey.killResult()
      testActorRef ! 7
      testActorRef ! 4
      testActorRef ! 6

      expectMsg(10 seconds, new ResponseError("Stop Error"))
      testActorRef ! new RequestAverage(8)
      testActorRef ! 1
      testActorRef ! 9
      testActorRef ! 2
      testActorRef ! 8
      testActorRef ! 3
      testActorRef ! 7
      testActorRef ! 4
      testActorRef ! 6

      expectMsg(5 seconds, new ResponseAverage(8, 5.0))

      system.stop(testActorRef)
    }
  }
}

class EvilMonkey(system: ActorSystem, systemName: String) {
  def restartActor(actorPath: String) {
    Thread.sleep(1000) // wait summarize,average actor to be initialized
    val act = system.actorSelection(actorPath) // will create new thread
    act ! RequestRestart // use thread created to send RequestRestart, so there maybe some delay
  }

  def killActor(actorPath: String) {
    Thread.sleep(1000)
    val act = system.actorSelection(actorPath)
    act ! Kill
  }

  def restartSummerize() {
    restartActor("user/%s/superAverage/summarize".format(systemName))
  }

  def killSummerize() {
    killActor("user/%s/superAverage/summarize".format(systemName))
  }

  def restartAverage() {
    restartActor("user/%s/superAverage/average".format(systemName))
  }

  def killAverage() {
    killActor("user/%s/superAverage/average".format(systemName))
  }

  def restartResult() {
    restartActor("user/%s/result".format(systemName))
  }

  def killResult() {
    killActor("user/%s/result".format(systemName))
  }

  def restartSupervisor() {
    restartActor("user/%s/superAverage".format(systemName))
  }

  def killSupervisor() {
    killActor("user/%s/superAverage".format(systemName))
  }

}