package ch03

import akka.actor.ActorSystem
import akka.actor.SupervisorStrategy.{Restart, Resume, Escalate}
import akka.testkit._
import akka.actor._
import ch02.ch02.StopSystemAfterAll
import ch3.{SupervisorAverageNew, SupervisorSummerizeNew, SummerizeActor}
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import scala.concurrent.duration._
/**
 * Created by anhhh11 on 19/07/2014.
 */
class SupervisorAvgTest
  extends TestKit(ActorSystem("testsystem")) //Create system for test
  with WordSpec // nested "something" must { "equal smt" in {} }
  with MustMatchers // provide `must`
  with StopSystemAfterAll  // provide `beforeAll`, `afterAll`
  with ImplicitSender
{
  "An exception in child" must {
    "restart one child even with mulitple children" in {
      val supervisorStrategy = OneForOneStrategy() {
        case _: IllegalArgumentException => Restart
        case _: IllegalStateException    => Restart
        case _: Exception                => Escalate
      }

      val probe = TestProbe()
      val testActorRef = system.actorOf(Props(new SupervisorAverageNew(probe.ref, supervisorStrategy)))
      testActorRef ! 4
      probe.expectMsg(4.0)

      testActorRef ! "stateError"
      Thread.sleep(1000)

      testActorRef ! "hehe"
      Thread.sleep(1000)

      //only SummerizeActor is Restart because one for one, mean one will be restart if its raise a exception
      testActorRef ! 4
      probe.expectMsg(2.0)

    }
    //restart all child for one's child exception
    "all child must restart if one raise exception" in {
      val supervisorStrategy = AllForOneStrategy(){
        case _ : IllegalArgumentException => Restart
        case _ : Exception => Escalate //Default Escalate is that it restart
      }
      val probe = TestProbe()
      val testActorRef = system.actorOf(Props(new SupervisorAverageNew(probe.ref,supervisorStrategy)))
      testActorRef ! 4
      probe.expectMsg(4.0)

      testActorRef ! "hehe"
      Thread.sleep(1000)

      testActorRef ! 4
      probe.expectMsg(4.0)
    }
  }
}