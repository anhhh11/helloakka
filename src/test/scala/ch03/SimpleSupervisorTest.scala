package ch03

import akka.actor.{Props, ActorSystem}
import akka.testkit.{TestProbe, TestKit}
import ch02.ch02.StopSystemAfterAll
import ch3.{SummerizeChild, SupervisorSummerize}
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Created by anhhh11 on 19/07/2014.
 */
class SimpleSupervisorTest
  extends TestKit(ActorSystem("testsystem")) //Create system for test
  with WordSpec // nested "something" must { "equal smt" in {} }
  with MustMatchers // provide `must`
  with StopSystemAfterAll  {// provide `beforeAll`, `afterAll` {
  "SummerizeChild" must {
    "resend value to sender" in {
      val probe = TestProbe()
      val testActorRef = system.actorOf(Props[SummerizeChild])
      testActorRef.tell(4, probe.ref)
      probe.expectMsg(4)
      testActorRef.tell(8, probe.ref)
      probe.expectMsg(12)

    }
  }
  "SupervisorSummerize" must {
    "Suppresses restart child when failing" in {
    val probe = TestProbe()
    val testActorRef = system.actorOf(Props[SupervisorSummerize])
    testActorRef.tell(4, probe.ref)
    probe.expectMsg(4)
    testActorRef ! "foo"
    testActorRef.tell(8, probe.ref)
    probe.expectMsg(12)
  }
}


}