package ch03

import akka.actor.{Props, ActorSystem}
import akka.testkit.{TestActorRef, TestProbe, TestKit}
import ch02.ch02.StopSystemAfterAll
import ch3.LifeCircleHook
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Created by anhhh11 on 19/07/2014.
 */
class LifeCircleHookTest
    extends TestKit(ActorSystem("testsystem")) //Create system for test
    with WordSpec // nested "something" must { "equal smt" in {} }
    with MustMatchers // provide `must`
    with StopSystemAfterAll  // provide `beforeAll`, `afterAll`
{
  "LifeCircleHook actor" must {
    "restart when error occur" in {
      val probe = TestProbe()
      val lifeCircleHook = TestActorRef(Props[LifeCircleHook])
      lifeCircleHook ! "restart"
      lifeCircleHook.tell("hello world!",probe.ref)
      probe.expectMsg("hello world!")
      system.stop(lifeCircleHook)
    }
  }
}
