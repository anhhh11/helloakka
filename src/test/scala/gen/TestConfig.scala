package gen

/**
 * Created by anhhh11 on 20/07/2014.
 */
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory

object TestConfig {
  val customConf = ConfigFactory.parseString("""
  akka.loglevel = "DEBUG"
  akka.debug.autoreceive = on
  akka.debug.lifecycle = on
  akka.actor.debug.receive = on
  my-dispatcher {
    type = Dispatcher
  }
  akka.actor.deployment {
    /averageComponent {
      dispatcher = my-dispatcher
    }
  }
  """)
  def system(name:String) = ActorSystem(name, ConfigFactory.load(customConf))

}
