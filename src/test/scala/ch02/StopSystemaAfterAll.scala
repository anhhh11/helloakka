package ch02

/**
 * Created by anhhh11 on 18/07/2014.
 */
package ch02
import org.scalatest.{ Suite, BeforeAndAfterAll }
import akka.testkit.TestKit

trait StopSystemAfterAll extends BeforeAndAfterAll{
 this: TestKit with Suite => //Type ascription
  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }
}

