package ch02

import akka.actor._
import akka.event.Logging.LogEvent
import akka.testkit._
import akka.util.Timeout
import EchoActorMessage._
import ch02.StopSystemAfterAll
import com.typesafe.config.ConfigFactory
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

import scala.concurrent.Await
import scala.util.{Failure, Success}

class EchoActorTest
  extends TestKit(EchoActorTest.testSystem) //Create system for test
  with WordSpec // nested "something" must { "equal smt" in {} }
  with MustMatchers // provide `must`
  with StopSystemAfterAll  // provide `beforeAll`, `afterAll`
{
  // test send msg -> expect a message back
  "An EchoActor" must {
    //All three test below are same but
    "Reply with the same message it receives" in {
      import akka.pattern.ask
      import scala.concurrent.duration._

      implicit val timeout = Timeout(1 seconds) //For Await
      implicit val ec = system.dispatcher //As Future context

      val echo = system.actorOf(Props(new EchoActor()), "echo1") //Create EchoActor

      val future = echo.ask("some message")
      future.onComplete {
        case Failure(_)   => //handle failure
        case Success(msg) => assert(msg == "some message")
      }

      Await.ready(future, Duration.Inf)
    }


    "Reply with the same message it receives (without ask)" in {
      //Better way simpler
      val echo = system.actorOf(Props(new EchoActor()), "echo2")
      echo.tell("some message", testActor)
      expectMsg("some message")
    }

    "Reply with the same message it receives (use TestKit builtin)" in {
      //Better way simpler
      val probe = TestProbe()
      val echo = system.actorOf(Props(new EchoActor()), "echo3")
      probe.send(echo,"some message")
      probe.expectMsg("some message")
    }

  }
  // test send msg -> receive response while
  "A Filtering Actor" must {
    "filter out particular messages" in {
      import EchoActorMessage._
      val filter = system.actorOf(Props(new EchoActor(nextActor = Some(testActor),bufferSize = 5)),
        "filter-1")
      filter ! Event(1)
      filter ! Event(2)
      filter ! Event(1)
      filter ! Event(3)
      filter ! Event(1)
      filter ! Event(4)
      filter ! Event(5)
      filter ! Event(5)
      filter ! Event(6)
      val eventIds = receiveWhile() {
        case Event(id) if id <= 5 => id
      }
      eventIds must be(List(1,2,3,4,5))
      expectMsg(Event(6))
    }
    "filter out particular messages using expectNoMsg - expect not message" in {
      import EchoActorMessage._
      val testProbe = TestProbe()
      val props = Props(new EchoActor(Some(testProbe.ref), 5))
      val filter = system.actorOf(props, "filter-3")
      filter ! Event(1)
      filter ! Event(2)
      testProbe.expectMsg(Event(1))
      testProbe.expectMsg(Event(2))
      filter ! Event(2)
      //testProbe.expectNoMsg() // too slow for testing so temperal breakpointing

    }
  }
  //thread dispatcher, deterministic execution order
  "The Echoer" must {
    //send msg -> expect log message equal ...
    "say Hello World! when a Greeting(\"World\") is sent to it" in {
      val dispatcherId = CallingThreadDispatcher.Id //http://www.packtpub.com/article/dispatchers-routers
      val echoer = system.actorOf(Props(new EchoActor(nextActor=Some(testActor))).withDispatcher(dispatcherId))
      EventFilter.info(message="Hello World !",occurrences = 1).intercept(
        echoer ! Greeting("World")
      )
      //echoer ! Greeting("World")
      expectMsg(Greeting("World"))
    }
    // send unknown,undefined msg -> expect
    "say something else and see what happens" in {
      val probe = TestProbe()
      val echoer = system.actorOf(Props(new EchoActor(nextActor=Some(probe.ref))))
      system.eventStream.subscribe(probe.ref,classOf[UnhandledMessage])
      //If message is not matched is will be forwarded to deadLetters
      val x = Array("Hello world")
      echoer ! x
      probe.expectMsg(UnhandledMessage(x,system.deadLetters,echoer))
    }
  }
  // test send msg -> expect match case
  "A echoer actors" must {
    "give two thanks when receiving 100 points twice" in {
      val probe = TestProbe()
      val echoer = system.actorOf(Props(new EchoActor()),"echoer-case")
      probe.send(echoer,Seq(100,100))
//      probe.expectMsg(Seq("Thank","Thank"))
      probe.expectMsgPF(){
        case response@Seq(_*) =>
          response.size must be(2)
      }
    }
  }
  //not implemented yet
  "A echoer actors" must {
    "do something .." in {
//      fail("Not implemented yet")
    }
  }
  // test actor change it inner state
  "A Silent Actor" must {
    "change internal state when it receives a string message ChangeInnerState" in {
      val slientActor = TestActorRef(new EchoActor())
      slientActor.underlyingActor.innerState must equal("original")
      slientActor ! "ChangeInnerState"
      slientActor.underlyingActor.innerState must equal("changed")
      slientActor ! NeedInnerState(testActor)
      expectMsg("changed")
    }
  }
  // test deadLetter
  "A echoer actors" must {
    "forward unknown message to deadLetters" in {
      val echoer = system.actorOf(Props(new EchoActor()),"echoer-unknown")
      echoer ! UnknownMessage
      expectNoMsg()
    }
  }
}

//Config test system
object EchoActorTest {
  val testSystem = {
    val config = ConfigFactory.parseString(
      """
      akka.event-handlers = ["akka.testkit.TestEventListener"]
    """.stripMargin)
    ActorSystem("testsystem",config)
    }
}

object EchoActorMessage{
  case class Event(id: Long)
  case class Greeting(who:String)
  case class NeedInnerState(whoNeed:ActorRef)
  case object UnknownMessage
}


class EchoActor(nextActor:Option[ActorRef] = None,bufferSize:Int = 0) extends Actor with ActorLogging{
  import EchoActorMessage._
  var lastMessages = Vector[Event]()
  var innerState = "original"
  def receive = {
    case NeedInnerState(whoNeed) =>
      whoNeed ! innerState

    case msg:String if msg=="ChangeInnerState" =>
      innerState = "changed"


    case point_seq@Seq(_*)
      if ((point_seq.size >= 2) && (point_seq(0)==100) && (point_seq(1)==100)) =>
        sender ! Seq("Thank","Thank")

    case Greeting(who) =>
      nextActor.foreach(_ ! Greeting(who))
      log.info("Hello " + who + " !")

    case msg: Event =>
      if (!lastMessages.contains(msg)){
        lastMessages  = lastMessages :+ msg
        nextActor.foreach(_ ! msg)
        if (lastMessages.size > bufferSize) {
          lastMessages = lastMessages.tail
        }
      }

    case msg : String=>
      sender ! msg
      //context.system.deadLetters forward msg
  }
}
