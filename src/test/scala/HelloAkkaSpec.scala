import org.scalatest.{WordSpec, BeforeAndAfterAll, FlatSpec}
import org.scalatest.concurrent._
import org.scalatest.matchers.{MustMatchers, ShouldMatchers}
import akka.actor.{ Actor, Props, ActorSystem }
import akka.testkit.{TestProbe, ImplicitSender, TestKit, TestActorRef}
import scala.concurrent.duration._

class HelloAkkaSpec(_system: ActorSystem)
  extends TestKit(_system)
  with ImplicitSender
  with ShouldMatchers
  with MustMatchers
  //with FlatSpec
  with WordSpec
  with BeforeAndAfterAll {

  def this() = this(ActorSystem("HelloAkkaSpec"))

  override def afterAll: Unit = {
    system.shutdown()
    system.awaitTermination(10.seconds)
  }
  "An HelloAkkaActor" should {
    "do like 1" in {
       val probe1 = TestProbe()
       val greeter = TestActorRef(Props[Greeter])
       within(300 milliseconds){
         probe1.send(greeter,Greeting("testkit"))
         val result = probe1.expectMsgType[Greeting]
         result.must(equal(Greeting("testkit")))
         probe1.expectMsg(Greeting("testkit"))
       }
    }
    "do like 2" in {

    }
  }
//  "An HelloAkkaActor" should "be able to set a new greeting" in {
//    val greeter = TestActorRef(Props[Greeter])
//    greeter ! Greeting("testkit")
//
//    //greeter.underlyingActor.asInstanceOf[Greeter].greeting should be("hello, testkit")
//  }
//
//  it should "be able to get a new greeting" in {
//    val greeter = system.actorOf(Props[Greeter], "greeter")
//    greeter ! Greeting("testkit")
//    //expectMsgType[Greeting].message.toString should be("hello, testkit")
//  }
}
