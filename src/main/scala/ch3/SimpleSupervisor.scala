package ch3

import akka.actor.SupervisorStrategy.{Restart, Resume}
import akka.actor.{Props, OneForOneStrategy, Actor}
import akka.actor.Actor.Receive

/**
 * Created by anhhh11 on 19/07/2014.
 */
class SummerizeChild extends Actor{
  println("SummerizeChild created")
  var sum = 0
  override def receive: Receive = {
    case value:Int =>
      sum+=value
      sender ! sum
  }
}
class SupervisorSummerize extends Actor {
  override def supervisorStrategy = OneForOneStrategy(){
    case _:IllegalArgumentException => Resume //mean continue to work
    case _:Exception => Restart //mean restart actor
  }
  val summerizeActor = context.actorOf(Props[SummerizeChild])
  def receive = {
    case any:AnyRef => summerizeActor.tell(any,sender)
  }
}
