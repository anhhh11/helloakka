package ch3

import akka.actor.{Actor, ActorLogging}

/**
 * Created by anhhh11 on 19/07/2014.
 */
class LifeCircleHook extends Actor with ActorLogging{
  println("Constructor")
  override def preStart() {
    println("PreStart")
  }
  override def postStop(){
    println("PostStop")
  }
  override def preRestart(reason:Throwable,
                           message:Option[Any]){
    println("PreRestart")
    super.preRestart(reason,message)
  }
  override def postRestart(reason:Throwable){
    println("PostRestart")
    super.postRestart(reason)
  }

  override def receive = {
    case "restart" =>
      println("receive restart")
      throw new IllegalArgumentException("force restart")
    case msg:AnyRef =>
      println("Receive")
      sender ! msg
  }
}
