package ch3

import akka.actor.Actor.Receive
import akka.actor.SupervisorStrategy.{Escalate, Stop, Restart}
import akka.actor._
import akka.event.LoggingReceive
import scala.concurrent.duration._
/**
 * Created by anhhh11 on 19/07/2014.
 */
object StateStore {
  var sumStore = 0
  var nrStore = 0
}

object SummerizeActor {
  def props(result:ActorRef) = Props(new SummerizeActor(result))
}

case object RequestRestart

class SummerizeActor(result:ActorRef) extends Actor {
  println("SummerizeActor created")
  var sum = 0

  override def preRestart(reason:Throwable,message:Option[Any]){
    println("SummerizeActor preRestart "+reason.getMessage)
    StateStore.sumStore = this.sum
    super.preRestart(reason,message)
  }
  override def postRestart(reason:Throwable){
    println("SummerizeActor postRestart"+reason.getMessage)
    sum = StateStore.sumStore
    super.postRestart(reason)
  }

  override def receive: Receive = LoggingReceive {
    case value:Int =>
      sum += value
      result ! sum
    case "reset" =>
      sum = 0
      result ! "reset"
      println("SummerizeActor RESET")
    case RequestRestart =>
      println("SummerizeActor requested restart")
      throw new IllegalStateException("requested restart")

    case "stateError" => throw new IllegalStateException("State problems")
    case _ => throw new IllegalArgumentException("Expect only integers")
  }
}
class AverageActor(result:ActorRef)  extends Actor {
  var nr = 0

  override def preRestart(reason:Throwable,message:Option[Any]){
    println("AverageActor preRestart "+reason.getMessage)
    StateStore.nrStore = this.nr
    super.preRestart(reason,message)
  }
  override def postRestart(reason:Throwable){
    println("AverageActor postRestart"+reason.getMessage)
    nr = StateStore.nrStore
    super.postRestart(reason)
  }

  override def receive: Actor.Receive = LoggingReceive{
    case sum:Int =>
      nr += 1
      val average = sum.toDouble / nr
      result ! average
    case "reset" =>
      nr = 0
      result ! "reset"
      println("AverageActor RESET")

    case RequestRestart =>
      println("AverageActor requested restart")
      throw new IllegalStateException("requested restart")

    case _ => throw new IllegalArgumentException("Expect only integers")
  }
}
class SupervisorSummerizeNew(result:ActorRef,strategy:SupervisorStrategy) extends Actor{
  val summerizeActor = context.actorOf(SummerizeActor.props(result))
  override val supervisorStrategy= strategy
  var restarted = false
  var childTerminated = false
  val watch = context.watch(summerizeActor) //Receive terminated when child actor is terminated
  println("SupervisorSummerize constructed")

  override def postRestart(reason:Throwable) {
    restarted = true
    println("SupervisorSummerize restarted because reason:"+reason.toString())
  }

  def receive = {
    case "getActor" => sender ! summerizeActor
    case x:String if x=="isRestarted" => sender ! restarted
    case "isChildTerminated" => sender ! childTerminated
    case Terminated(actorRef) =>
      if (actorRef==summerizeActor){
        childTerminated = true
        println("Child terminated")
      } else {
        println("Old Child is terminated")
      }
    case any : AnyRef =>
      if (!childTerminated)
        summerizeActor ! any
      else
        result ! "error"
  }

}

class SupervisorAverageNew(result:ActorRef,strategy:SupervisorStrategy) extends Actor{
  val averageActor = context.actorOf(Props(new AverageActor(result)))
  val summerizeActor = context.actorOf(SummerizeActor.props(averageActor))
  override val supervisorStrategy = strategy
  var restarted = false
  override def postRestart(reason: Throwable) {
    restarted = true
    System.out.println("postRestart reason=" + reason.getMessage)
  }
  def receive = {
    case "getActor"    => sender ! summerizeActor
    case "isRestarted" => sender ! restarted
    case any: AnyRef   => summerizeActor ! any
  }

}


object SupervisorAverageNewStreateged {
  def props(result:ActorRef) = Props(new SupervisorAverageNewStreateged(result))
}
class SupervisorAverageNewStreateged(result:ActorRef) extends Actor{

  val averageActor = context.actorOf(Props(new AverageActor(result)),"average")
  val summerizeActor = context.actorOf(SummerizeActor.props(averageActor),"summarize")

  override val supervisorStrategy = AllForOneStrategy(5, 1 minute){
    case _:IllegalStateException => Restart
    case _:IllegalArgumentException => Restart
    case _:ActorKilledException => Stop
  }
  override def preStart() {
    context.watch(summerizeActor)
  }

  override def postStop() {
    context.unwatch(summerizeActor)
  }

  override def preRestart(reason: Throwable,
                          message: Option[Any]) {
    System.out.println("SupervisorAverageNewStreateged preRestart"
      + reason.getMessage)
    super.preRestart(reason, message)
  }
  override def postRestart(reason: Throwable) {
    System.out.println("SupervisorAverageNewStreateged postRestart "
      + reason.getMessage)
    super.postRestart(reason)
  }

  def receive = LoggingReceive {
    case "getActor" => sender ! summerizeActor
    case Terminated(child) =>
        throw new IllegalStateException(
          "Child actor shouldn't be ternimated")
    case RequestRestart =>
      throw new IllegalStateException("requested restart")

    case any:AnyRef => summerizeActor ! any
  }

}

case class ResponseAverage(requestNr:Int,average:Double)
case class ResponseError(msg:String)
case class RequestAverage(nr:Int)
class ResultChild() extends  Actor {
  var requester : Option[ActorRef] = None
  var requestNr = 0
  var nrReceived = 0

  System.out.println("ResultChild created")

  override def preRestart(reason: Throwable,
                          message: Option[Any]) {
    System.out.println("ResultChild preRestart")

    requester.foreach(req => { //<co id="ch3-compl-result-6" />
      req ! new ResponseError("Stop Error")
      requester = None
    })
  }
  override def postStop() {
    System.out.println("ResultChild postStop")
  }


  def receive = LoggingReceive {
    case average : Double =>
      if (nrReceived >= 0 ){
        nrReceived += 1
        requester.foreach(req =>{
          if (nrReceived==requestNr){
            req ! new ResponseAverage(requestNr,average)
            requester = None
            requestNr = 0
          } else {
            println("ResultChild average %.2f msg %d needed %d".format(average,
              nrReceived,requestNr))
          }
        })
      } else {
        println("ResultChild missing reset")
      }
    case "reset" => nrReceived = 0
    case RequestAverage(nr) =>
      requester = Some(sender)
      requestNr = nr
      if (nrReceived>0)
        nrReceived = -1
      println("ResultChild request average needed %d".format(
        requestNr))

    case RequestRestart =>
      throw new IllegalStateException("requested restart")

    case _ => throw new IllegalArgumentException("Unexpected message")
  }

}


class SupervisorAvgCmp() extends Actor{
  val resultActor = context.actorOf(Props[ResultChild],"result")
  val supervisorAvgActor = context.actorOf(SupervisorAverageNewStreateged.props(resultActor),"superAverage")

  override val supervisorStrategy = AllForOneStrategy(){
    case _:IllegalStateException => Restart
    case _:ActorKilledException => Restart
    case _:Exception => Escalate
  }

  def receive = LoggingReceive {
    case "getActor" => sender ! resultActor
    case req : RequestAverage =>
      supervisorAvgActor ! "reset"
      resultActor.forward(req)
    case RequestRestart =>
      throw new IllegalStateException("requested restart")
    case any:AnyRef => supervisorAvgActor ! any
  }

}


