package message_example

/**
 * Created by anhhh11 on 20/07/2014.
 */
import akka.actor._
object TestMsgApp extends App{
  val system = ActorSystem("FutureInActor")
  val po = system.actorOf(Props[ProcessOrder])
  po ! 456
  Thread.sleep(1000)
  system.shutdown()
}
