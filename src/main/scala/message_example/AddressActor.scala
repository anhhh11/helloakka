package message_example

/**
 * Created by anhhh11 on 20/07/2014.
 */
import akka.actor._
import Msg._
class AddressActor extends Actor {

  def receive = {
    case userId: Int =>
      sender ! new Msg.Address(userId, "Munish Gupta", "Sarjapura Road", "Bangalore, India")
      //Just one userId maybe search in db
  }
}