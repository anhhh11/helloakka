package message_example

/**
 * Created by anhhh11 on 20/07/2014.
 */
import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import message_example.Msg._
import akka.pattern.pipe
import scala.concurrent.duration._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class ProcessOrder extends Actor{
  val orderActor = context.actorOf(Props[OrderActor])
  val addressActor = context.actorOf(Props[AddressActor])
  val orderAggregate = context.actorOf(Props[OrderAggregate])
  implicit val timeout = Timeout(5 seconds)
  def receive = {
    case userId : Int =>
      val aggResult : Future[OrderHistory] =
      for {
        order <- orderActor.ask(userId).mapTo[Order]
        address <- addressActor.ask(userId).mapTo(manifest[Msg.Address])
      } yield OrderHistory(order,address)
      aggResult.pipeTo(orderAggregate)
  }
}
