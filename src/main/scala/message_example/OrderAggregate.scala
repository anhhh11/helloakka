package message_example

/**
 * Created by anhhh11 on 20/07/2014.
 */
import akka.actor._
import Msg._
class OrderAggregate extends Actor{
  def receive = {
    case orderHistory: OrderHistory =>
      println(orderHistory.toString)
  }
}
