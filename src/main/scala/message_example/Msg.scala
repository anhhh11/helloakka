package message_example

/**
 * Created by anhhh11 on 20/07/2014.
 */
object Msg {
  case class Order(userId: Int, orderNo: Int, amount: Float, noOfItems: Int)
  case class Address(userId: Int, fullName: String, address1: String, address2: String)
  case class OrderHistory(order: Order, address: Address)
}
