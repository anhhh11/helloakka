package message_example

/**
 * Created by anhhh11 on 20/07/2014.
 */
import akka.actor._
import Msg._
class OrderActor extends Actor {

  def receive = {
    case userId: Int =>
      sender ! new Order(userId, 123, 345, 5)
  }
}