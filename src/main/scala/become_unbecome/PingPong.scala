package become_unbecome

/**
 * Created by anhhh11 on 20/07/2014.
 */

import akka.actor.Actor.Receive
import akka.actor._

case object Ping
case object Pong

class PingPong  extends Actor{
  import context._
  def pinger : Receive = {
    case Pong => become(ponger)
    case "listen" => println("PING!")
  }
  def ponger : Receive = {
    case Ping => become(pinger)
    case "listen" => println("PONG!")
  }
  override def receive: Receive = {
    case Ping => become(pinger)
    case Pong => become(ponger)
    case "listen" => println("NOTHING!")
    case _ => throw new IllegalArgumentException("What?I am only ping or pong ;)")
  }
}
