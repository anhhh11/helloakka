package become_unbecome

/**
 * Created by anhhh11 on 20/07/2014.
 */

import akka.actor.Actor.Receive
import akka.actor._
import com.typesafe.config
import scala.concurrent.duration._
case object PING
case object PONG
class HotSwapPingPong extends Actor{
  import context._
  var count = 0

  override def receive: Receive = {
    case PING =>
      println("PING")
      count += 1
      Thread.sleep(100)
      self ! PONG
      become({
        case PONG =>
          println("PONG")
          count += 1
          Thread.sleep(100)
          self ! PING
          unbecome()
      },discardOld = false)
      if(count>10) context.stop(self)
  }
}
