package become_unbecome

/**
 * Created by anhhh11 on 20/07/2014.
 */
import akka.actor._
object TestPingPong extends App{
  val system = ActorSystem("PingPongTester")
  val testActorRef = system.actorOf(Props[PingPong])
  val testHotSwap = system.actorOf(Props[HotSwapPingPong])
  testHotSwap ! PING
  testActorRef ! "listen"
  testActorRef ! "listen"
  testActorRef ! "???"
  testActorRef ! Ping
  testActorRef ! "listen"
  testActorRef ! "listen"
  testActorRef ! Pong
  testActorRef ! "listen"
  testActorRef ! "listen"
}
