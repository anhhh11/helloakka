package dispatcher

import akka.actor.Actor

/**
 * Created by anhhh11 on 20/07/2014.
 */
class MsgEchoActor extends Actor{
  @volatile var messageProcessed:Int = 0
  def receive = {
    case message =>
      messageProcessed += 1
      Thread.sleep(100)
      println("Path:%s ; ThreadName:%s ; Message: %s".
        format(self.path,Thread.currentThread().getName(),messageProcessed))
  }
}
