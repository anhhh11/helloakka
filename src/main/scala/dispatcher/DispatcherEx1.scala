package dispatcher

/**
 * Created by anhhh11 on 20/07/2014.
 */

import akka.routing.{FromConfig, RoundRobinRouter}
import com.typesafe.config.ConfigFactory
import akka.actor._
import router.BurstyRouter

object DispatcherEx1 extends App{
  val system = ActorSystem("DispatcherEx1",
    ConfigFactory.load().getConfig("MyDispatcherExample"))
  val actor = system.actorOf(Props[MsgEchoActor]
    .withDispatcher("CallingThreadDispatcher")
    //.withRouter(FromConfig()),name="myRandomRouterActor") //Round
  .withRouter(new BurstyRouter(10,5)))
  0 until 400 foreach { i => actor ! i }
  system.wait()
  system.shutdown()
}
