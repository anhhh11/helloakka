package router

import akka.actor.SupervisorStrategy
import akka.routing.{Destination, Route, RouteeProvider, RouterConfig}
import akka.dispatch.Dispatchers
import akka.actor._
/**
 * Created by anhhh11 on 20/07/2014.
 */
class BurstyRouter(nrOfInstances:Int,messageBurst:Int) extends RouterConfig{
  override def createRoute(routeeProvider: RouteeProvider): Route = {
    val routees = new Array[ActorRef](nrOfInstances)
    var actorPos = 0
    @volatile var messageCount = 0
    for (i<-0 until nrOfInstances){
      routees.update(i,routeeProvider.context.actorOf(routeeProvider.routeeProps))
    }
    routeeProvider.registerRoutees(routees.toVector)

    {
      case (sender,message) =>
        var actor = routeeProvider.routees(actorPos)
        synchronized { messageCount += 1}
        if (messageCount == messageBurst) {
          actorPos += 1
          synchronized {
            messageCount = 0
          }
          if (actorPos == nrOfInstances)
            actorPos = 0
        }
        List(Destination(sender,actor))
    }

  }

  override def routerDispatcher: String = Dispatchers.DefaultDispatcherId

  override def supervisorStrategy: SupervisorStrategy = SupervisorStrategy.defaultStrategy


}
