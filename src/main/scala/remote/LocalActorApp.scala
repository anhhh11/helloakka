package remote

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory
/**
 * Created by anhhh11 on 20/07/2014.
 */
object LocalActorApp extends App{
  val config = ConfigFactory.load().getConfig("LocalSys")
  val system = ActorSystem("LocalActorApp",config)
  var clientActor = system.actorOf(Props[LocalActor])
  clientActor ! "Hello"
  Thread.sleep(10000)
  system.shutdown()
}
