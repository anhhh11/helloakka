package remote

import akka.actor.Actor
/**
 * Created by anhhh11 on 20/07/2014.
 */
class RemoteActor extends Actor {
  def receive = {
    case msg : String =>
      sender ! (msg + "got something")
  }
}
