package remote

import akka.actor._
import akka.util.Timeout
import akka.pattern.ask
import scala.concurrent.Await
import scala.concurrent.duration._
import akka.remote.RemoteScope
import akka.actor.Address
import akka.actor.Deploy

/**
 * Created by anhhh11 on 20/07/2014.
 */
class LocalActor extends Actor with ActorLogging{
  val remoteActor = context.actorSelection("akka.tcp://RemoteNodeApp@127.0.0.1:2553/user/remoteActor")
  implicit val timeout = Timeout(5 seconds)
  def receive = {
    case message : String =>
      val future = (remoteActor ? message).mapTo[String]
      val result = Await.result(future,timeout.duration)
      log.info("Message received from Server -> {}",result)
  }
}
