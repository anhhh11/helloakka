package remote

import akka.actor.{Props, ActorSystem}
import akka.kernel.Bootable
import com.typesafe.config.ConfigFactory

/**
 * Created by anhhh11 on 20/07/2014.
 */
object RemoteActorApp extends App{
  val system = ActorSystem("RemoteNodeApp",ConfigFactory.load().getConfig("RemoteSys"))
  system.actorOf(Props[RemoteActor],name="remoteActor")

}
