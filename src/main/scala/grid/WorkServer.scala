package grid

/**
 * Created by anhhh11 on 22/07/2014.
 */
import akka.actor._
import akka.remote.AssociationEvent
import com.typesafe.config._
import akka.dispatch.PriorityGenerator
import grid.ServerMessage._

object WorkServer extends App{
  val config = ConfigFactory.load().getConfig("WorkServerSys")
  val system =
    ActorSystem("WorkServerActor",config)
  val workSchedular =
    system.actorOf(Props[WorkSchedularActor],name="workSchedular")
  val jobController =
    system.actorOf(Props(new JobControllerActor(workSchedular)),name="jobController")
  val registerWorker =
    system.actorOf(Props(new RegisterWorkerActor(jobController)),name="registerWorker")
  val remoteClientEventListener =
    system.actorOf(Props(new RemoteClientEventListener(jobController)),name="remoteClientEventListener")

  system.eventStream.subscribe(remoteClientEventListener,classOf[AssociationEvent])


}
