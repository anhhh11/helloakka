package grid

/**
 * Created by anhhh11 on 22/07/2014.
 */
import akka.actor._
import akka.actor.Address
import akka.remote._
import akka._
import com.typesafe.config._
import grid.ServerMessage.StartWorker

object WorkClient extends App{
  val argsList = args.toList
  val configName = argsList.head
  val systemName = "WorkerSys"
  val config = ConfigFactory.load().getConfig(configName)
  val system = ActorSystem(systemName,config)

  val registerRemoteWorker = system.actorSelection(config.getString("register-actor-server-path"))

  val akkaProtocol= "akka."+config.getString("usage-protocal")
  val ip = config.getConfig("akka.remote.netty."+config.getString("usage-protocal")).getString("hostname")
  val port = config.getConfig("akka.remote.netty."+config.getString("usage-protocal")).getInt("port")

  val myAddr = new Address(akkaProtocol, systemName,ip, port)
  registerRemoteWorker! new StartWorker(myAddr.toString)
}
