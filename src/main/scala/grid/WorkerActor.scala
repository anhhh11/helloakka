package grid

import akka.actor.{ActorLogging, Actor}
import akka.actor.Actor.Receive

/**
 * Created by anhhh11 on 22/07/2014.
 */
class WorkerActor extends Actor with ActorLogging{
  import ServerMessage._
  override def receive: Receive = {
    case STOP => context.stop(self)
    case Task(id) =>
      if (messageProcessedCounter==10){
        sender ! ENOUGH
        context.stop(self)
        context.system.shutdown()
      } else {
        log.info(s"Work Packet from server -> ${id}")
        messageProcessedCounter += 1
        sender ! new TaskFinished(id)
      }
  }
  var instanceCount = 0
  var messageProcessedCounter = 0
  override def preStart(){
    instanceCount += 1
    log.info(s"Starting worker actor hash code #${self.hashCode()} number #${instanceCount}")
  }
  override def postStop(){
    instanceCount -= 1
    log.info(s"Stopping worker actor hash code #${self.hashCode()} number #${instanceCount}")
  }
}
