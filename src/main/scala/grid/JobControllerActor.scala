package grid

/**
 * Created by anhhh11 on 22/07/2014.
 */

import akka.actor.Actor.Receive
import akka.actor._
import akka.remote._
import ServerMessage._
import akka.remote.routing.RemoteRouterConfig
import akka.routing.RoundRobinRouter

import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayBuffer

class JobControllerActor(workSchedular:ActorRef) extends Actor with ActorLogging{
  val workersAdressMap = HashMap[String,Address]()
  var workersRouter : Option[ActorRef] = None
  var count = 0
  override def receive: Receive = {
    //From RegisterWorkerActor
    case StartWorker(workerAddress) =>
      log.info(s"Received worker requested addding from $workerAddress")
      addWorker(workerAddress)
      workSchedular ! "give me task"
    case StopWorker(workerAddress) =>
      log.info(s"Received worker requested removing from$workerAddress")
      removeWorker(workerAddress)
    //From Worker
    case TaskFinished(taskNumber) =>
      log.info(s"Task #$taskNumber finished")
      workSchedular ! "give me another task"

    case ENOUGH =>
      val senderPath = sender.path.toString()
      log.info(s"Worker at ${senderPath} has complete given tasks. Now it will shut down")
      removeWorker(senderPath)
    //From TaskScheduler
    case SendWork() =>
      log.info("About to send work to workers")
      sendWorkToEachWorker()
    case _ =>
      throw new IllegalArgumentException("Illegal argument")
  }

  override def preStart = {
    log.info(s"Starting Job Controller with hashCode #${this.hashCode()}")
  }
  override def postStop = {
    log.info(s"Stopping Job Controller with hashCode #${this.hashCode()}")
  }

  private def sendWorkToEachWorker(){
    workersRouter match {
      case None =>
        log.info("No workers registered right now!")
        workSchedular ! 100
      case Some(wr) =>
        for(i<-0 until workersAdressMap.size){
          count += 1
          wr ! Task.create(count)
        }
    }
  }
  private def stopAllWorker(){
    workersRouter.foreach { wr =>
      for (i<-0 until workersAdressMap.size){
        wr ! STOP
      }
    }
  }
  private def reinitRouterWorker(){
    val remoteRouter = RemoteRouterConfig(RoundRobinRouter(workersAdressMap.size),
      workersAdressMap.values)
    workersRouter = Some(context.system.actorOf(Props[WorkerActor].withRouter(remoteRouter)))
  }
  private def addWorker(workerAdress:String) {
    stopAllWorker()
    workersAdressMap += workerAdress -> AddressFromURIString(workerAdress)
    reinitRouterWorker()
  }
  private def removeWorker(workerAdress:String){
    stopAllWorker()
    workersAdressMap -= workerAdress
    if (workersAdressMap.size>0)
      reinitRouterWorker()
    else
      workersRouter = None
  }
}
