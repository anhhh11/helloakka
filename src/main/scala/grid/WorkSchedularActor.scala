package grid

/**
 * Created by anhhh11 on 22/07/2014.
 */
import akka._
import akka.actor.{ActorLogging, Actor}
import akka.actor.Actor.Receive
import ServerMessage._
import scala.concurrent.duration._
class WorkSchedularActor extends Actor with ActorLogging{
  lazy implicit val dispatcher = context.system.dispatcher
  override def receive: Receive = {
    case someMsg:String =>
      log.info("Received work sending request")
      sendWorkAfter(50.millisecond)
    case delay:Int =>
      log.info("Retry sending work")
      sendWorkAfter(delay.millisecond)
  }
  private def sendWorkAfter(milsec:FiniteDuration) =
    context.system.scheduler.scheduleOnce(milsec,sender,new SendWork())
}
