package grid

/**
 * Created by anhhh11 on 22/07/2014.
 */

import akka.actor.Actor.Receive
import akka.actor._
import grid.ServerMessage.StartWorker

class RegisterWorkerActor(jobController:ActorRef) extends Actor with ActorLogging{
  override def receive: Receive = {
    case msg@StartWorker(worker) =>
      log.info(s"Received Registration Info from ${worker}")
      jobController ! msg
    case _ =>
      throw new IllegalAccessException("Invalid message type received")
  }
}
