package grid

/**
 * Created by anhhh11 on 22/07/2014.
 */
import akka.remote._
import akka.actor._
object ServerMessage {
  case class StartWorker(workerAddress:String)
  case class StopWorker(workerAddress:String)
  case class SendWork()
  case class Task(id:Int)
  object Task {
    def create(id:Int)  = new Task(id)
  }
  case class TaskFinished(taskNumber:Int)
  case object STOP
  case object ENOUGH
}
