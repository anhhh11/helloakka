package grid

import akka.actor.{PoisonPill, Address, ActorSystem}
import akka.dispatch.{BoundedPriorityMailbox, PriorityGenerator, UnboundedPriorityMailbox}
import com.typesafe.config.Config
import grid.ServerMessage.{TaskFinished, ENOUGH}
import scala.concurrent.duration._
/**
 * Created by anhhh11 on 22/07/2014.
 */
class MyUnboundedPriorityMailbox(settings:ActorSystem.Settings,config:Config)
  extends UnboundedPriorityMailbox(
    PriorityGenerator{
      case Address => 0 //HIGHEST PRIORITY
      case ENOUGH => 2
      case TaskFinished => 1
      case PoisonPill => 4
      case otherwise => 5
    }
  )
//, 1000, 1000.millisecond