package grid

/**
 * Created by anhhh11 on 22/07/2014.
 */

import akka.actor.Actor.Receive
import akka.actor._
import akka.remote.AssociationEvent
import grid.ServerMessage.StopWorker

class RemoteClientEventListener(jobController:ActorRef) extends Actor with ActorLogging{
  override def receive: Receive = {
    case event : Exception=>
      log.error(s"Receive exception:${event.getCause} ${event.getMessage}")
      //val errorAdress = event.getRemoteAddress.toString
      //log.error(s"Connection error: ${event.getClass} occur at ${errorAdress }")
      //log.info("Now shutting down this worker")
      //jobController ! StopWorker(errorAdress)
  }
}
