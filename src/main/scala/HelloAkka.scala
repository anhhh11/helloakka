import akka.actor.{Props, ActorSystem, ActorLogging, Actor}
import akka.actor.Actor.Receive

//step 2
case class Greeting(who:String)

//step 4
class Greeter extends Actor with ActorLogging{
  override def receive: Receive = {
    case Greeting(who) => log.info(who)
  }
}

object HelloAkka extends App {
  val system = ActorSystem("JustGreeting")
  val greeter = system.actorOf(Props[Greeter],"greeter")
  greeter ! Greeting("hello akka")
  greeter ! Greeting("first step")
}
