package ch2

import akka.actor.{ActorRef, ActorSystem, Props}

/**
 * Created by anhhh11 on 18/07/2014.
 */
object BadProps extends App{
  val system = ActorSystem("firstSystem")
  case class Arena(seats: Int) //Immutable -> Good
  val arena = Arena(1000)
  val okProps = Props(new PrintingOffice(100, arena))
  val printRefOk = system.actorOf(okProps, "printingOffice")
  printRefOk.tell("world",ActorRef.noSender)
}