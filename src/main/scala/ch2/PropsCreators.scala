package ch2

import akka.actor.Props

/**
 * Created by anhhh11 on 18/07/2014.
 */
object PropsCreators {
  //<start id="ch02-propscreators"/>
  val props1 = Props()
  val props2 = Props[MyActor]
  object PrintingOffice {
    def apply(batch:Int,seats:Int) : Props = Props(new PrintingOffice(batch,seats))
  }
  val props3 = Props(new PrintingOffice(batch = 100, seats = 1500)) //ONLY USE OUTSIDE ACTOR
  val props6 = PrintingOffice(batch=100,seats=1500) //FOR CREATE AS CHILD ACTOR
  //val props3 = Props(new MyActor) //Bad practice
  //val props4 = Props(creator = new MyActor) //Bad practice
  //val props5 = Props().withCreator(new MyActor) //Bad practice
  //<end id="ch02-propscreators"/>
}

