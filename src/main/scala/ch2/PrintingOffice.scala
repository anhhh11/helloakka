package ch2

import akka.actor.Actor.Receive
import akka.actor.{ActorLogging, Actor}
import ch2.BadProps.{Arena}

/**
 * Created by anhhh11 on 18/07/2014.
 */
class PrintingOffice(batch: Int, seats: Int) extends Actor with ActorLogging{
  def this(batchSize: Int, arena: Arena) = //Apply when using new PrintingOffice(...)
    this(batchSize, arena.seats)

  override def receive: Receive = {
    case msg => log.info(msg.toString)
  }
}