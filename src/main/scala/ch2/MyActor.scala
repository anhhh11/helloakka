package ch2

import akka.actor.Actor

/**
 * Created by anhhh11 on 18/07/2014.
 */
class MyActor extends Actor {
  def receive = {
    case msg => // do something with the message
  }
}
